const {request, response} = require('express');
const  express = require('express');
const app = express();
const port = 8000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.get('/home', (request, response) => {
    response.send('Welcome to the home page');
});


let users = [];
app.get('/users', (request, response) => {

    if(request.body.username !== '' && request.body.password !== ''){
        users.push(request.body)
        console.log(users);

        response.send(users);
    } else {
        response.send('Please input BOTH username and password')
    };
});


let message
app.delete('/delete-user', (request,response) => {

    for (let i = 0; i <users.length; i++){
        if (request.body.username == users[i].username){
            users[i].username = request.body.username

            message = `User ${request.body.username} has been deleted!`

            break
        } else {
            message = "User does not exist"
        }
    };
    response.send(message);
});

app.listen(port, () => console.log(`Server is now running at localhost: ${port}`));